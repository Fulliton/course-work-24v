const { Menu } = require('electron');
const electron = require('electron');
const app = electron.app;
const Tray = electron.Tray;
const BrowserWindow = electron.BrowserWindow
const dialog = electron.dialog 
const path = require('path');
const url = require('url');
const fs = require('fs');

const template = [
    {
        label: 'Изменить',
        submenu: [
            {
                label: 'Назад',
                role: 'undo'
            },
            {
                label: 'Вперёд',
                role: 'redo'
            },
            {
                type: 'separator'
            },
            {
                label: 'Вырезать',
                role: 'cut'
            },
            {
                label: 'Копировать',
                role: 'copy'
            },
            {
                label: 'Вставить',
                role: 'paste'
            },
            {
                label: 'Удалить',
                role: 'delete'
            },
            {
                label: 'Выбрать всё',
                role: 'selectall'
            }
        ]
    },
    {
        label: 'Просмотр',
        submenu: [
            {
                label: 'Обновить',
                accelerator: 'CmdOrCtrl+R',
                click(item, focusedWindow) {
                    if (focusedWindow) focusedWindow.reload()
                }
            },
            {
                label: 'Режим разработчика',
                accelerator: process.platform === 'darwin' ? 'Alt+Command+I' : 'Ctrl+Shift+I',
                click(item, focusedWindow) {
                    if (focusedWindow) focusedWindow.webContents.toggleDevTools()
                }
            },
            {
                type: 'separator'
            },
            {
                label: 'Сброс размера окна',
                role: 'resetzoom'
            },
            {
                label: 'Приблизить',
                role: 'zoomin'
            },
            {
                label: 'Отдалить',
                role: 'zoomout'
            },
            {
                type: 'separator'
            },
            {
                label: 'В полный экран',
                role: 'togglefullscreen'
            }
        ]
    },
    {
        label: 'Окно',
        role: 'window',
        submenu: [
            {
                label: 'Свернуть',
                role: 'minimize'
            },
            {
                label: 'Закрыть',
                role: 'close'
            }
        ]
    },
    {
        label: 'Помощь',
        role: 'help',
        submenu: [
            {
                label: 'Разработчики',
                click() { require('electron').shell.openExternal('https://artyshko.ru') }
            }
        ]
    },
]

if (process.platform === 'darwin') {
    const name = app.getName()
    template.unshift({
        label: name,
        submenu: [
            {
                role: 'about'
            },
            {
                type: 'separator'
            },
            {
                role: 'services',
                submenu: []
            },
            {
                type: 'separator'
            },
            {
                role: 'hide'
            },
            {
                role: 'hideothers'
            },
            {
                role: 'unhide'
            },
            {
                type: 'separator'
            },
            {
                role: 'quit'
            }
        ]
    })
    // Edit menu.
    template[1].submenu.push(
        {
            type: 'separator'
        },
        {
            label: 'Speech',
            submenu: [
                {
                    role: 'startspeaking'
                },
                {
                    role: 'stopspeaking'
                }
            ]
        }
    )
    // Window menu.
    template[3].submenu = [
        {
            label: 'Close',
            accelerator: 'CmdOrCtrl+W',
            role: 'close'
        },
        {
            label: 'Minimize',
            accelerator: 'CmdOrCtrl+M',
            role: 'minimize'
        },
        {
            label: 'Zoom',
            role: 'zoom'
        },
        {
            type: 'separator'
        },
        {
            label: 'Bring All to Front',
            role: 'front'
        }
    ]
}

const menu = Menu.buildFromTemplate(template)
Menu.setApplicationMenu(menu)