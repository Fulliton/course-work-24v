class Arc {
  constructor(fVertex, sVertex) {
    this.weight = null;
    this.vertex = [fVertex, sVertex];
  }

  set ["vertexes"](vertexes) {
    if (vertexes.length != 2) {
      console.log(vertexes.length);
      throw new Error("Number of vertexes shoud be equal 2");
    } else {
      this.vertex = vertexes;
    }
  }

  draw(color) {
    ctx.strokeStyle = color;
    ctx.fillStyle = color;
    ctx.beginPath();
    ctx.moveTo(this.vertex[0].coords.x, this.vertex[0].coords.y);
    ctx.lineTo(this.vertex[1].coords.x, this.vertex[1].coords.y);
    ctx.stroke();
    ctx.closePath();

    this.vertex[0].clear();
    this.vertex[1].clear();

    this.vertex[0].draw(color);
    this.vertex[1].draw(color);
  }

  contains(vertex) {
    for (let i = 0; i < this.vertex.length; i++) {
      // console.log(vertex.name + " = " + this.vertex[i].name);
      if (vertex.name == this.vertex[i].name) {
        return true;
      }
    }

    return false;
  }
}
