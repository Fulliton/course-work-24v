const prompt = require('electron-prompt');
canvas = document.getElementById("canvas");
ctx = canvas.getContext("2d");

canvas.width = 500;
canvas.height = 500;
canvas.lineWidth = 2;

let VERTEX_RADIUS = 20;
let selectedVertex = null;
let firstVertex = null;
let graph;

window.onload = function() {
  ctx.translate(canvas.width / 2, canvas.height / 2);
  ctx.font = "bold 14pt Arial";

  graph = new Graph();

  graph.addVertex(new Vertex("v1", {x: 0,   y: 120 }, VERTEX_RADIUS));
  graph.addVertex(new Vertex("v2", {x: 85,  y: 85 }, VERTEX_RADIUS));
  graph.addVertex(new Vertex("v3", {x: 120,  y: 0  }, VERTEX_RADIUS));
  graph.addVertex(new Vertex("v4", {x: 85,  y: -85}, VERTEX_RADIUS));
  graph.addVertex(new Vertex("v5", {x: 0,   y: -120}, VERTEX_RADIUS));
  graph.addVertex(new Vertex("v6", {x: -85, y: -85}, VERTEX_RADIUS));
  graph.addVertex(new Vertex("v7", {x: -120, y: 0  }, VERTEX_RADIUS));
  graph.addVertex(new Vertex("v8", {x: -85, y: 85 }, VERTEX_RADIUS));

  graph.draw("black");

  canvas.addEventListener("mouseup", graph.click);
  canvas.addEventListener("mousemove", graph.hover);
}
