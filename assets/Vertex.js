class Vertex {
  constructor(name, coords, radius) {
    this.name = name;
    this.picked = false;
    this.status = function() {
      this.clear();
      this.draw("black");
    }
    this.coords = coords;
    this.radius = radius;
  }

  draw(color) {
    ctx.beginPath();
    ctx.strokeStyle = color;
    ctx.fillStyle = color;
    ctx.textAlign = "center";
    ctx.textBaseline = "middle";
    ctx.arc(this.coords.x ,this.coords.y, this.radius, 0, 360, false);
    ctx.fillText(this.name, this.coords.x, this.coords.y);
    ctx.stroke();
    ctx.closePath();
  }

  onPicked() {
    ctx.beginPath();
    ctx.strokeStyle = "red";
    ctx.fillStyle = "red";
    ctx.textAlign = "center";
    ctx.textBaseline = "middle";
    ctx.arc(this.coords.x ,this.coords.y, this.radius, 0, 360, false);
    ctx.fillText(this.name, this.coords.x, this.coords.y);
    ctx.stroke();
    ctx.closePath();
    this.picked = true;
  }

  clear() {
    ctx.beginPath();
    ctx.fillStyle = "white";
    ctx.strokeStyle = "white";
    ctx.arc(this.coords.x, this.coords.y, this.radius + 0.5, 0, 360, false);
    ctx.fill();
    ctx.stroke();
  }
}
