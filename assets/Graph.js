class Graph {
  constructor() {
    this.vertexes = [];
    this.arcs = [];
    this.picked = null;
    this.firstVertex = null;
  }

  addVertex(vertex) {
    this.vertexes.push(vertex);
  }

  draw(color) {
    for (let i = 0; i < this.vertexes.length; i++) {
      this.vertexes[i].draw(color);
    }
  }

  connect(fVertex, sVertex) {
    canvas.removeEventListener("mouseup", this.click);

    if (!this.containsArc(fVertex, sVertex) &&
        fVertex.name != sVertex.name) {
      let arc = new Arc(fVertex, sVertex);

      prompt({
        title: 'Введите длину ребра',
        label: 'Длина ребра',
        value: '',
        inputAttrs: { // attrs to be set if using 'input'
          maxlength: 4,
          onkeydown: 'if (!((event.keyCode > 47 && event.keyCode < 58) && !event.shiftKey) && event.keyCode != 8) { return false;}',
        },
        type: 'input', // 'select' or 'input, defaults to 'input'
      }).then((r) => {
        if (r != null && r != "") {
          arc.weight = Number(r);
          arc.draw("black")
          this.arcs.push(arc);
        }

        this.picked = null;
        fVertex.picked = false;
        sVertex.picked = false;

        fVertex.clear();
        sVertex.clear();

        fVertex.draw("black");
        sVertex.draw("black");

        canvas.addEventListener("mouseup", this.click);
      });
    } else {
      this.picked = null;
      fVertex.picked = false;
      sVertex.picked = false;

      fVertex.clear();
      sVertex.clear();

      fVertex.draw("black");
      sVertex.draw("black");

      canvas.addEventListener("mouseup", this.click);
    }
  }

  containsArc(fVertex, sVertex) {
    let contains = false;

    if (this.firstVertex == null) {
      this.firstVertex = fVertex;
    } else {
      for (let i = 0; i < this.arcs.length; i++) {
        if (!this.arcs[i].contains(fVertex) &&
            !this.arcs[i].contains(sVertex)) {
          contains = true;
        } else {
          contains = false;
          break;
        }
      }

      for (let i = 0; i < this.arcs.length; i++) {
        if (this.arcs[i].contains(fVertex) &&
            this.arcs[i].contains(sVertex)) {
          contains = true;
        }
      }
    }

    return contains;
  }

  hover(e) {
    let x = e.layerX - canvas.width / 2;
    let y = e.layerY - canvas.height / 2;

    for (let i = 0; i < graph.vertexes.length; i++) {
      if (!graph.vertexes[i].picked) {
        if (Math.pow(x - graph.vertexes[i].coords.x, 2) +
            Math.pow(y - graph.vertexes[i].coords.y, 2) <=
            Math.pow(graph.vertexes[i].radius, 2)) {
          graph.vertexes[i].clear();
          graph.vertexes[i].draw("grey");
        } else {
          graph.vertexes[i].clear();
          graph.vertexes[i].draw("black");
        }
      }
    }
  }

  click(e) {
    let x = e.layerX - canvas.width / 2;
    let y = e.layerY - canvas.height / 2;

    for (let i = 0; i < graph.vertexes.length; i++) {
      if (Math.pow(x - graph.vertexes[i].coords.x, 2) +
          Math.pow(y - graph.vertexes[i].coords.y, 2) <=
          Math.pow(graph.vertexes[i].radius, 2)) {
        if (graph.picked == null) {
          graph.picked = graph.vertexes[i];
          graph.vertexes[i].clear();
          graph.vertexes[i].onPicked();
        } else {
          graph.vertexes[i].clear();
          graph.vertexes[i].onPicked();
          graph.connect(graph.picked, graph.vertexes[i]);
        }
      }
    }
  }

  minimalArc() {
    let min = Number.MAX_SAFE_INTEGER;
    for (let i = 0; i < this.arcs.length; i++) {
      console.log(this.arcs[i].weight);
      if (this.arcs[i].weight < min) {
        min = this.arcs[i].weight;
        oneVert = this.arcs[i].vertex[0].name;
        twoVert = this.arcs[i].vertex[1].name;
        console.log(this.arcs[i].vertex[1].name);
      }
      console.log(min);
    }

    if (min == Number.MAX_SAFE_INTEGER) {
      document.getElementById("Answer").innerHTML = "Graph hasn't any arcs!";
    } 
    else {
      document.getElementById("Answer").innerHTML = "Вес ребра: " + min + ", Вершины: " + oneVert + "->" + twoVert;
    }
  }
}
