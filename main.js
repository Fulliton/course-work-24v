const electron = require('electron');

const app = electron.app;
const BrowserWindow = electron.BrowserWindow;
const Menu = electron.Menu;
const dialog = electron.dialog;

const path = require('path');
const url = require('url');

//Electron

let mainWindow;

function createWindow () {
  // Create the browser window.
  
  mainWindow = new BrowserWindow({
    width: 800,
    height: 600,
    minWidth: 600,
    minHeight: 400,
    show: false
  });
  mainWindow.loadURL(url.format({
    pathname: path.join(__dirname, 'src/index.html'),
    protocol: 'file:',
    slashes: true,
  }));
  mainWindow.on('closed', function () {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    mainWindow = null
  });
  // mainWindow.webContents.openDevTools();
  require('./mainmenu');
  mainWindow.once('ready-to-show', () => {
    mainWindow.show()
  })
}


app.on('ready', createWindow);

// Quit when all windows are closed.
app.on('window-all-closed', function () {

  if (process.platform !== 'darwin') {
    app.quit()
  }

});

app.on('activate', function () {
  if (mainWindow === null) {
    createWindow()
  }

});
